function counterFactory() 
{
    return { 'increment':function increment(n){return n+1},
             'decrement':function decrement(n){return n-1}}
}

console.log(counterFactory().decrement(7))
console.log(counterFactory().increment(5))







//the variable n is can be used as a constant time the function to be called
function limitFunctionCallCount(cb, n) {
    function toLimit()
    {
        for(let index=0;index<n;index++)
            cb()
    }
    return toLimit()
}

limitFunctionCallCount(cb=()=>{console.log("hello")},5)
    
    





let obj={input1:[1,2,3,4,5],input2:[1,2,3,4],input3:[1,2,3],input4:[1,2,4,5],input5:[1,3,4,5],input6:[1,2,3,4,5],input7:[1,2,3],input8:[1,2]}

function cacheFunction(cb) 
{   let cache={}
    return function(arr,val){
        if(cache[obj[val]]===undefined)
        {
            cache[obj[val]]=cb(arr)
            console.log(cache[obj[val]])
        }
        else{
            console.log(cache[obj[val]])
        }
        }
}

const fun=cacheFunction(cb=(array)=>{let sum=0
    for(let index=0;index<array.length;index++)
        sum+=array[index]
    return sum})

for(let val in obj)
    fun(obj[val],val)

