const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

// Complete the following underscore functions.
// Reference http://underscorejs.org/ for examples.




function keys(obj) {
  // Retrieve all the names of the object's properties.
  // Return the keys as strings in an array.
  // Based on http://underscorejs.org/#keys
  return Object.keys(obj)
}
console.log(Keys(testObject))






function values(obj) {
  // Return all of the values of the object's own properties.
  // Ignore functions
  // http://underscorejs.org/#values
  return Object.values(obj)
}
console.log(Keys(testObject))




const arr={start:5,end:12}
function mapObject(obj, cb) {
  // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
  // http://underscorejs.org/#mapObject
      var arrkey=Object.keys(arr)
    var arrval=Object.values(arr)
    for(var i=0;i<arrkey.length;i++)
    {
        arr[arrkey[i]]=cb(arrval[i])
    }
    return arr
}
console.log(mapObject(arr,cb=(ele)=>{return ele+5}))







function pairs(obj) {
  // Convert an object into a list of [key, value] pairs.
  // http://underscorejs.org/#pairs
   let arr=[]
        for(var x in obj)
        {   
            var arr2=[]
            arr2.push(x)
            arr2.push(testObject[x])
            arr.push(arr2)
        }
        return arr
}
console.log(pairs(testObject))






/* STRETCH PROBLEMS */

function invert(obj) {
  // Returns a copy of the object where the keys have become the values and the values the keys.
  // Assume that all of the object's values will be unique and string serializable.
  // http://underscorejs.org/#invert
    let arr={}
    for(var k in testObject)
        arr[testObject[k]]=k
    return arr
}
console.log(invert(testObject))







let test2={name: 'Clark Kent'}
function defaults(obj, defaultProps) {
  // Fill in undefined properties that match properties on the `defaultProps` parameter object.
  // Return `obj`.
  // http://underscorejs.org/#defaults
for(var qw in defaultProps)
    {
        if(obj[qw]===undefined)
        {
            obj[qw]=defaultProps[qw]
        }
    }
    return obj
}
console.log(defaults(test2,testObject))
