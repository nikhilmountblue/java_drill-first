const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code. 



//const items=2
function each(elements, cb) 
{
  for(let i=0;i<elements.length;i++)
  	cb(elements[i])
}
each(items,cb=(ele)=>{console.log(ele)})




function map(elements, cb) {
  const maparr=[]
  for(let i=0;i<items.length;i++)
        maparr.push(cb(items[i]))
  return maparr
}
console.log(map(items,cb=(ele)=>{return ele*2}))




function reduce(elements, cb, startingValue) {
  let x=startingValue || 0
  for(let i=0;i<elements.length;i++)
  {x=cb(elements[i],x)}
  return x
}
console.log(reduce(items,cb=(ele,stat)=>{return ele+stat}))




function find(elements, cb) {
    var findarr=[]
    for(let i=0;i<elements.length;i++)
    {
        if(cb(elements[i]))
        {return true}
        else if(i===elements.length-1)
        {return false}
    }   
}
console.log(find(items,cb=(ele)=>{return ele===2}))





function filter(elements, cb) {
  let filterarr=[]
    for(let i=0;i<elements.length;i++)
    {
        if(cb(elements[i]))
        {filterarr.push(elements[i])}
    }
    return filterarr
}




const nestedArray = [1, [2], [[3]], [[[4]]], [[['q']]], [[[["asd"]]]], [[{qw:12,we:23}]]]; // use this to test 'flatten'
function flatten(elements) {
  let recur =(elements,r)=>{
      if(!r)
      {r = []}
 
      for(var i=0; i<elements.length; i++)
      {
           if(Array.isArray(elements[i]))
           {recur(elements[i],r);}
           else
           {r.push(elements[i]);}
       }
       return r;
   }
   return recur(elements)
}
console.log(flatten(nestedArray))

